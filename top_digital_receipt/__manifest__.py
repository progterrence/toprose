# -*- coding: utf-8 -*-
{
    'name': 'POS Digital Receipt',
    'category': 'POS Digital Receipt',
    'summary': 'POS Digital Receipt',
    'author': 'Sunflower IT',
    'website': 'http://www.sunflowerweb.nl',
    'version': '14.0.1.0.0',
    'depends': [
        'toprose_mtech_sms',
        'point_of_sale'
    ],
    "data": [],
    'installable': True,
}
