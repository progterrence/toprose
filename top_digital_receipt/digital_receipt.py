# -*- coding: utf-8 -*-
import time
from datetime import datetime
from odoo import models, fields, api, _
from odoo.exceptions import UserError
import requests


class TopDigitalReceipt(models.Model):
    _inherit = "pos.order"

    
    def action_paid(self):
        ret = super(TopDigitalReceipt, self).action_paid()
        # send digital receipt
        self.send_sms_receipt()

        return ret

    
    def send_sms_receipt(self):
        sms = self.env['mtech.sms'].search(
            [('sms_type', '=', 'digital_receipt'), ('sms_state', '=', True)])
        if sms:
            MESSAGE = eval(sms[0].sms_msg)
            MSISDN = ""
            if self.partner_id:
                partner = self.partner_id
                if partner.phone:
                    if partner.phone.startswith('0'):
                        MSISDN = '254' + partner.phone[1:]
                    elif partner.phone.startswith('+254'):
                        MSISDN = partner.phone[1:]
                    elif partner.phone.startswith('7'):
                        MSISDN = '254' + partner.phone
                    elif partner.phone.startswith('254'):
                        MSISDN = partner.phone
                    else:
                        raise UserError(_("Phone Number is not valid."))

                link = "http://ke.mtechcomm.com/bulkAPIV2/?user=thetoprose&" \
                       "pass=6db81a58913e1c920277ae814084cb662a416882&" \
                       "messageID=Promotion&" \
                       "shortCode=TheTopRose&" \
                       "MSISDN={}&" \
                       "MESSAGE={}".format(MSISDN, MESSAGE)
                r = requests.post(link)
                # print(r.status_code, r.reason)
                #print MSISDN, MESSAGE
