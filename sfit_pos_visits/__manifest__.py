# -*- coding: utf-8 -*-
{
    'name': 'POS Visits',
    'category': 'Point of Sale',
    'summary': 'This module allows customers earn loyalty points.',
    'author': 'Sunflower IT',
    'website': 'http://www.sunflowerweb.nl',
    'version': '14.0.1.0.0',
    'depends': [
        'base',
        'point_of_sale',
    ],
    "data": [
        'views/res_partner.xml',
        'views/point_of_sale_assets.xml',
        'views/pos_order_view.xml'
    ],
    'qweb': ['static/src/xml/*.xml'],
    'installable': True,
}
