# -*- coding: utf-8 -*-

from odoo import models, fields, api


class ResPartner(models.Model):
    _inherit = "res.partner"

    pos_order_ids = fields.One2many('pos.order', 'partner_id')
    total_visits = fields.Float('Total Visits',
        compute='_compute_visits')
    remaining_visits = fields.Float('Remaining Visits',
        compute='_compute_visits')
    redeemed_visits = fields.Float('Redeemed Visits', compute='_compute_visits')

    
    def _compute_visits(self):
        for this in self:
            visits = this.pos_order_ids.filtered(
                    lambda x: x.state not in ['draft', 'invoiced'])
            this.total_visits = len(visits)
            this.redeemed_visits = sum(
                visits.mapped('redeemed_visits'))
            this.remaining_visits = this.total_visits - sum(
                visits.mapped('redeemed_visits'))

