# -*- coding: utf-8 -*-

from odoo import models, fields, api


class PosOrder(models.Model):
    _inherit = "pos.order"

    redeemed_visits = fields.Integer('Redeemed Visits')
