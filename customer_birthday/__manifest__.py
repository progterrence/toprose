# -*- coding: utf-8 -*-
{
    'name': 'Customer Birthday',
    'summary': 'Birthdays for customers',
    'version': '14.0.1.0.4',
    'author': 'Terrence Nzaywa',
    'license': 'AGPL-3',
    'maintainer': 'Terrence Nzaywa',
    'support': 'nza.terrence@gmail.com',
    'category': 'Tools',
    'depends': [
         'sale'
    ],
    'data': [
        'customer_birthday.xml'
    ],
    'demo': [],
    'installable': True
}
