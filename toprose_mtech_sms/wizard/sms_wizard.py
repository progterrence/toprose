# -*- coding: utf-8 -*-
# © 2016 Sunflower IT (http://sunflowerweb.nl)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

import time
from datetime import datetime
from odoo import models, fields, api, _
from odoo.exceptions import UserError
import requests

class SendSMSWizard(models.TransientModel):
    _name = 'wizard.mtech.sms'
    _description = 'Wizard for mass SMS'

    sms_msg = fields.Text(string='SMS Message')

    
    def send_mass_sms(self):
      active_ids = self.env.context.get('active_ids', False)
      if not active_ids:
        return
      customers = self.env['res.partner'].browse(active_ids)
      for partner in customers:
          if not partner.name:
            raise UserError(_("Some Contacts have no Name"))
          if not partner.phone:
            raise UserError(_("{} has no Phone Number".format(partner.name)))
          if not self.sms_msg:
            raise UserError(_("SMS to be sent is not set"))
          first_name = partner.name.split(' ', 1)[0]
          MESSAGE = "Hi {}, ".format(first_name)
          MESSAGE += self.sms_msg
          if partner.phone.startswith('0'):
            MSISDN = '254' + partner.phone[1:]
          elif partner.phone.startswith('+254'):
            MSISDN = partner.phone[1:]
          elif partner.phone.startswith('7'):
            MSISDN = '254' + partner.phone
          elif partner.phone.startswith('254'):
            MSISDN = partner.phone
          else:
            raise UserError(_("Phone Number is not valid."))

          link = "http://52.15.88.116/bulkAPIV2/?user=thetoprose&" \
                 "pass=6db81a58913e1c920277ae814084cb662a416882&" \
                 "messageID=Promotion&" \
                 "shortCode=TheTopRose&" \
                 "MSISDN={}&" \
                 "MESSAGE={}".format(MSISDN, MESSAGE)
          r = requests.post(link)
          print(r.status_code, r.reason)
          print(MSISDN, MESSAGE)
