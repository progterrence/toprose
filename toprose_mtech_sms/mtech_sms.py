# -*- coding: utf-8 -*-
#
import time
from datetime import datetime
from odoo import models, fields, api, _
from odoo.exceptions import UserError
import requests

SMS_TYPE = [
    ("custom", "Custom SMS"),
    ("digital_receipt", "Digital Receipt"),
    ("referral", "Customer Referral"),
]

class MtechApiSms(models.Model):
    _name = "mtech.sms"

    sms_name = fields.Char(string='Name')
    sms_type = fields.Selection(SMS_TYPE, string='SMS Type', required=True)
    sms_msg = fields.Text(string='SMS Message')
    sms_state = fields.Boolean(string="Active to be Send")

    
    def send_sms_to_all_customers(self):
      customers = self.env['res.partner'].search([('customer', '=', True)])
      for partner in customers:
        if not partner.name:
          raise UserError(_("Some Contacts have no Name"))
        if not partner.phone:
          raise UserError(_("{} has no Phone Number".format(partner.name)))
        if not self.sms_msg:
          raise UserError(_("SMS to be sent is not set"))
        first_name = partner.name.split(' ', 1)[0]
        MESSAGE = "Hi {}, ".format(first_name)
        MESSAGE += self.sms_msg
        if partner.phone.startswith('0'):
          MSISDN = '254' + partner.phone[1:]
        elif partner.phone.startswith('+254'):
          MSISDN = partner.phone[1:]
        elif partner.phone.startswith('7'):
          MSISDN = '254' + partner.phone
        elif partner.phone.startswith('254'):
          MSISDN = partner.phone
        else:
          raise UserError(_("Phone Number is not valid."))

      link = "http://52.15.88.116/bulkAPIV2/?user=thetoprose&" \
             "pass=6db81a58913e1c920277ae814084cb662a416882&" \
             "messageID=Promotion&" \
             "shortCode=TheTopRose&" \
             "MSISDN={}&" \
             "MESSAGE={}".format(MSISDN, MESSAGE)
      # r = requests.post(link)
      # print(r.status_code, r.reason)
      print(MSISDN, self.sms_msg)

