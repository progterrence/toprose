
{
    'name': 'MTECH SMS',
    'summary': 'All modules',
    'version': '14.0.1.0.4',
    'author': 'Terrence Nzaywa',
    'license': 'AGPL-3',
    'maintainer': 'Terrence Nzaywa',
    'support': 'nza.terrence@gmail.com',
    'depends': [

    ],
    'demo': [],
    'data': [
        # 'mtech_sms.xml',
        # 'mtech_sms.xml',
    ],
    'installable': True
}
