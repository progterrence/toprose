# -*- coding: utf-8 -*-
{
    'name': 'TOP Referral Program',
    'category': 'TOP Referral Program',
    'summary': 'TOP Referral Program',
    'author': 'Sunflower IT',
    'website': 'http://www.sunflowerweb.nl',
    'version': '14.0.1.0.0',
    'depends': [
        'sale',
        'sfit_pos_loyalty',
        'top_customer_phone',
        'toprose_mtech_sms',
        'point_of_sale'
    ],
    'qweb': ['static/src/xml/pos.xml'],
    'data': [
        'customer_referral.xml',
        # 'mtech_sms.xml',
    ],
    'installable': True,
}
