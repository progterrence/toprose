# -*- coding: utf-8 -*-
#
import time
from datetime import datetime
from odoo import models, fields, api, _
from odoo.exceptions import UserError
import requests


class TopReferralProgram(models.Model):
    _inherit = "res.partner"

    referred_by = fields.Many2one('res.partner', 'Referred By')
    referred_by_phone = fields.Char('Referred By Phone')

    @api.model
    def create(self, vals):
        ret = super(TopReferralProgram, self).create(vals)

        #check if has been referred and award loyalty points
        if 'phone' in vals or 'mobile' in vals:
            if 'referred_by_phone' in vals and vals['referred_by_phone']:
                partner= self.env['res.partner'].search(
                    ['|', ('phone', '=', vals['referred_by_phone']), ('mobile', '=', vals['referred_by_phone'])]
                )
                if partner:
                    ret.referred_by = partner[0].id
                    self.env['loyalty.point'].create({
                        'partner_id': partner[0].id,
                        'points': 50
                    })
                    partner[0].send_sms_referral()

        return ret

    
    def send_sms_referral(self):
        sms = self.env['mtech.sms'].search([('sms_type', '=', 'referral'), ('sms_state', '=', True)])
        if sms:
            MESSAGE = eval(sms[0].sms_msg)
            partner = self
            if partner.phone:
	            if partner.phone.startswith('0'):
	              MSISDN = '254' + partner.phone[1:]
	            elif partner.phone.startswith('+254'):
	              MSISDN = partner.phone[1:]
	            elif partner.phone.startswith('7'):
	              MSISDN = '254' + partner.phone
	            elif partner.phone.startswith('254'):
	              MSISDN = partner.phone
	            else:
	              raise UserError(_("Phone Number is not valid."))

	            link = "http://52.15.88.116/bulkAPIV2/?user=thetoprose&" \
	                   "pass=6db81a58913e1c920277ae814084cb662a416882&" \
	                   "messageID=Promotion&" \
	                   "shortCode=TheTopRose&" \
	                   "MSISDN={}&" \
	                   "MESSAGE={}".format(MSISDN, MESSAGE)
	            r = requests.post(link)
	            # print(r.status_code, r.reason)
	            print(MSISDN, MESSAGE)



