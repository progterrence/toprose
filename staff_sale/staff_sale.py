# -*- coding: utf-8 -*-
#
import time
from datetime import datetime
from odoo import models, fields, api, _
from odoo.exceptions import UserError

TYPE = [
    ('sale', 'Sale'),
    ('deduction', 'Deduction')
]

class staff_sale(models.Model):
    _name = "staff.sale"
    _description = "Staff sale and commission"
    _order = "id desc"

    order_id = fields.Many2one('pos.order', 'Ref Order')
    # order_id = fields.Char('Ref Order')
    type = fields.Selection(TYPE, 'Type', default='sale')
    name = fields.Char('POS Name')
    price = fields.Char('Service Price')
    commission = fields.Float(string="Commission", store=True)
    staff_id = fields.Many2one('hr.employee', 'Staff Name')
    product_id = fields.Many2one('product.product')
    partner_id = fields.Many2one(related='order_id.partner_id')
    customer_name = fields.Char(related='partner_id.name', string='Customer Name')
    customer_phone = fields.Char(related='partner_id.phone', string='Customer Phone')
    customer_mobile = fields.Char(related='partner_id.mobile', string='Customer Mobile')
    staff_name = fields.Char(related='staff_id.name', string='Staff Name')
    date = fields.Date('Date')

class pos_order_ext(models.Model):
    _inherit = "pos.order"

    @api.model
    def create(self, values):
        res = super(pos_order_ext, self).create(values)
        if values.get('pos_reference'):
            if values['lines']:
                order_id = self.env['pos.order'].search([('pos_reference', '=', values['pos_reference'])])
                # if order_id:
                #     exists_order_id = self.pool['staff.sale'].search(cr, uid, [['order_id', '=', order_id.id]], context=context)
                #     if not exists_order_id:
                for line in values['lines']:
                    if 'staff_id' in line[2]:
                        if line[2]['commission40'] == 1:
                            commission = ((line[2]['price_unit'] * line[2]['qty']) - line[2]['discount']) * 0.4
                        else:
                            commission = ((line[2]['price_unit'] * line[2]['qty']) - line[2]['discount']) / 2

                            # staff = self.env['hr.employee'].browse([line[2]['staff_id']])
                            # order_product = self.env['product.product'].browse([line[2]['product_id']])
                            # assoc_products = self.env['product.template'].browse(order_product.assoc_product_ids.ids)
                            # staff_products = self.env['hr.employee'].browse([line[2]['staff_id']]).hr_product_ids
                            # product_list = []
                            # staff_product_list = []
                            #
                            # if staff_products:
                            #     for p in staff_products:
                            #         if p.product_id:
                            #             product_list.append(p.product_id.id)
                            # if assoc_products:
                            #     for p in assoc_products.ids:
                            #         if p:
                            #             staff_product_list.append(p)
                            #
                            # common_products = set(product_list).intersection(staff_product_list)
                            # if common_products:
                            #     commission = ((line[2]['price_unit'] * line[2]['qty']) - line[2]['discount']) / 2
                            # else:
                            #     commission = ((line[2]['price_unit'] * line[2]['qty']) - line[2]['discount']) * 0.4

                        staff_sale_vals = {
                            'name':values['name'],
                            # 'order_id': values['pos_reference'],
                            'order_id': order_id.id,
                            'price': (line[2]['price_unit'] * line[2]['qty']) - line[2]['discount'],
                            'commission': commission,
                            'staff_id':line[2]['staff_id'],
                            'product_id':line[2]['product_id'],
                            'date':time.strftime('%Y-%m-%d %H:%M:%S')
                        }
                        self.env['staff.sale'].create(staff_sale_vals)
        return res

    
    def refund(self):
        """Create a copy of order  for refund order"""
        PosOrder = self.env['pos.order']
        current_session = self.env['pos.session'].search(
            [('state', '!=', 'closed'), ('user_id', '=', self.env.uid)],
            limit=1)
        if not current_session:
            raise UserError(_(
                'To return product(s), you need to open a session that will be used to register the refund.'))
        for order in self:
            # check if orderlines had staff and unlink
            for orderline in order.lines:
                if 'staff_id' in orderline.read()[0]:
                    if orderline.staff_id:
                        staff_sale_ids = self.env['staff.sale'].search([('order_id', '=', orderline.order_id.id),('staff_id', '=', orderline.staff_id.id)])
                        staff_sale_ids.unlink()
            clone = order.copy({
                # ot used, name forced by create
                'name': order.name + _(' REFUND'),
                'type': 'return',
                'session_id': current_session.id,
                'date_order': fields.Datetime.now(),
                # 'pos_reference': order.pos_reference,
            })
            PosOrder += clone

        for clone in PosOrder:
            for order_line in clone.lines:
                order_line.write({'qty': -order_line.qty})
        return {
            'name': _('Return Products'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'pos.order',
            'res_id': PosOrder.ids[0],
            'view_id': False,
            'context': self.env.context,
            'type': 'ir.actions.act_window',
            'target': 'current',
        }
