# -*- coding: utf-8 -*-
{
    'name': 'Staff Sales',
    'category': 'Staff Sales',
    'summary': 'This module allows customers earn loyalty points.',
    'author': 'Sunflower IT',
    'website': 'http://www.sunflowerweb.nl',
    'version': '14.0.1.0.0',
    'depends': [
        'sale',
        'hr',
        'point_of_sale',
    ],
    "data": [
        'security/staff_sale_security.xml',
        'security/ir.model.access.csv',
        'templates.xml',
        'staff_sale.xml',
    ],
    'qweb': ['static/src/xml/staff_sale.xml'],
    # "post_init_hook": "post_init_hook",
    'installable': True,
}
