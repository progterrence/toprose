# -*- coding: utf-8 -*-
{
    'name': 'POS Customer Receipt',
    'summary': 'POS Customer Receipt',
    'version': '14.0.1.0.4',
    'author': 'Terrence Nzaywa',
    'license': 'AGPL-3',
    'maintainer': 'Terrence Nzaywa',
    'support': 'nza.terrence@gmail.com',
    'depends': [
         'sfit_pos_loyalty'
    ],
    'data': [],
    'qweb': ['static/src/xml/pos_receipt.xml'],
    'demo': [],
    'installable': True
}
