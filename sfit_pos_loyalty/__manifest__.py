# -*- coding: utf-8 -*-
{
    'name': 'POS Loyalty',
    'category': 'Point of Sale',
    'summary': 'This module allows customers earn loyalty points.',
    'author': 'Sunflower IT',
    'website': 'http://www.sunflowerweb.nl',
    'version': '9.0.1.0.0',
    'depends': [
        'base',
        'point_of_sale',
        # 'account_accountant',
        'sale'
    ],
    "data": [
        'security/ir.model.access.csv',
        # 'data/email_template.xml',
        'views/redeem_loyalty_points.xml',
        'views/loyalty_config_view.xml',
        'views/loyalty_view.xml',
        'views/product_product.xml',
        'views/pos_category.xml',
        # 'views/pos_config.xml',
        'views/pos_order.xml',
        # 'views/res_partner.xml',
        'views/sfit_pos_loyalty.xml'
    ],
    'qweb': ['static/src/xml/pos.xml'],
    "post_init_hook": "post_init_hook",
    'installable': True,
}
