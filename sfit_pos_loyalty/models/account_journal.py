# -*- coding: utf-8 -*-

from odoo import models, api


class AccountJournal(models.Model):
    _inherit = "account.journal"

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        if self._context.get('loyalty_jr'):
            if self._context.get('journal_ids') and \
                    self._context.get('journal_ids')[0] and \
                    self._context.get('journal_ids')[0][2]:
                args += [['id', 'in', self._context.get('journal_ids')[0][2]]]
            else:
                return False
        return super(AccountJournal, self).name_search(name, args=args,
                                                       operator=operator,
                                                       limit=limit)
