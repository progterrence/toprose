# -*- coding: utf-8 -*-

from . import account_journal
from . import loyalty_config
from . import loyalty_point
from . import pos_config
from . import pos_order
from . import product_category
from . import product_product
from . import redeem_loyalty_point
from . import res_partner

