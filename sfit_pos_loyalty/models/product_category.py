# -*- coding: utf-8 -*-

from odoo import models, fields


class ProductCategory(models.Model):
    _inherit = "pos.category"

    loyalty_point = fields.Integer("Loyalty Point")