from odoo import models, fields
from datetime import datetime


class RedeemLoyaltyPoint(models.Model):
    _name = "redeem.loyalty.point"
    _order = 'id desc'
    _rec_name = "redeemed_pos_order_id"

    redeemed_pos_order_id = fields.Many2one("pos.order", string="Order")
    partner_id = fields.Many2one('res.partner', 'Member', readonly=1)
    redeemed_amount_total = fields.Float('Redeemed Amount', readonly=1)
    redeemed_date = fields.Datetime('Date', readonly=1, default=datetime.now())
    redeemed_point = fields.Float('Point', readonly=1)
