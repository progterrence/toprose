# -*- coding: utf-8 -*-

from odoo import models, fields
from datetime import datetime


class LoyaltyPoint(models.Model):
    _name = "loyalty.point"
    _order = 'id desc'
    _rec_name = "pos_order_id"

    pos_order_id = fields.Many2one("pos.order", string="Order", readonly=0)
    partner_id = fields.Many2one('res.partner', 'Member', readonly=0)
    amount_total = fields.Float('Total Amount', readonly=0)
    date = fields.Datetime('Date', readonly=0, default=datetime.now())
    points = fields.Float('Point', readonly=0)
