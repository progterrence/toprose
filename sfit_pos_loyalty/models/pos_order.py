# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
import logging

_logger = logging.getLogger(__name__)


class PosOrder(models.Model):
    _inherit = "pos.order"

    total_loyalty_earned_points = fields.Float("Earned Loyalty Points")
    total_loyalty_earned_amount = fields.Float("Earned Loyalty Amount")
    total_loyalty_redeem_points = fields.Float("Redeemed Loyalty Points")
    total_loyalty_redeem_amount = fields.Float("Redeemed Loyalty Amount")

    @api.model
    def _order_fields(self, ui_order):
        res = super(PosOrder, self)._order_fields(ui_order)
        res.update({
            'total_loyalty_earned_points': ui_order.get(
                'loyalty_earned_point') or 0.00,
            'total_loyalty_earned_amount': ui_order.get(
                'loyalty_earned_amount') or 0.00,
            'total_loyalty_redeem_points': ui_order.get(
                'loyalty_redeemed_point') or 0.00,
            'total_loyalty_redeem_amount': ui_order.get(
                'loyalty_redeemed_amount') or 0.00,
        })
        return res


    @api.model
    def _process_order(self, order):
        res = super(PosOrder, self)._process_order(order)

        _order = self.env['pos.order'].browse(res)
        if _order.session_id.config_id.enable_pos_loyalty and _order.partner_id:
            loyalty_setting_id = self.env[
                'loyalty.config.settings'].sudo().search([], order="id desc",
                                                         limit=1)
            if loyalty_setting_id:
                if loyalty_setting_id.points_based_on and order.get(
                        'loyalty_earned_point'):
                    amount_total = (float(order.get('loyalty_earned_point')) *
                                    loyalty_setting_id.to_amount) / \
                                   loyalty_setting_id.points
                    point_vals = {
                        'pos_order_id': _order.id,
                        'partner_id': _order.partner_id.id,
                        'points': order.get('loyalty_earned_point'),
                        'amount_total': amount_total
                    }
                    loyalty = self.env['loyalty.point'].create(point_vals)
                    if loyalty and _order.partner_id.send_loyalty_mail:
                        try:
                            template_id = self.env[
                                'ir.model.data'].get_object_reference(
                                'sfit_pos_loyalty',
                                'email_template_pos_loyalty')
                            template_obj = self.env['mail.template'].browse(
                                template_id[1])
                            template_obj.send_mail(loyalty.id, force_send=True,
                                                   raise_exception=True)
                        except Exception as e:
                            _logger.error('Unable to send email for order %s',
                                          e)
                if order.get('loyalty_redeemed_point'):
                    redeemed_amt_tt = self._calculate_amount_total_by_points(
                        loyalty_setting_id, order.get(
                            'loyalty_redeemed_point'))
                    redeemed_vals = {
                        'redeemed_pos_order_id': _order.id,
                        'partner_id': _order.partner_id.id,
                        'redeemed_amount_total': redeemed_amt_tt,
                        'redeemed_point': order.get('loyalty_redeemed_point'),
                    }
                    self.env['redeem.loyalty.point'].create(redeemed_vals)
        return res

    def _calculate_amount_total_by_points(self, loyalty_config, points):
        return (float(
            points) * loyalty_config.to_redeem_amount) / loyalty_config.points

    def get_point_from_category(self, categ_id):
        if categ_id.loyalty_point:
            return categ_id.loyalty_point
        elif categ_id.parent_id:
            self.get_point_from_category(categ_id.parent_id)
        return False

    def _calculate_loyalty_points_by_order(self, loyalty_config):
        if loyalty_config.point_calculation:
            earned_points = self.amount_total * \
                            loyalty_config.point_calculation / 100
            amount_total = (earned_points *
                            loyalty_config.to_amount) / loyalty_config.points
            return {
                'points': earned_points,
                'amount_total': amount_total
            }
        return False

    
    def refund(self):
        order = super(PosOrder, self).refund()
        loyalty_points = self.env['loyalty.point']
        refund_order_id = self.browse(order.get('res_id'))
        if refund_order_id:
            loyalty_points.create({
                'pos_order_id': refund_order_id.id,
                'partner_id': self.partner_id.id,
                'points': refund_order_id.total_loyalty_redeem_points,
                'amount_total': refund_order_id.total_loyalty_redeem_amount,

            })
            loyalty_points.create({
                'pos_order_id': refund_order_id.id,
                'partner_id': self.partner_id.id,
                'points': refund_order_id.total_loyalty_earned_points * -1,
                'amount_total': refund_order_id.total_loyalty_earned_amount * -1,

            })
            total_loyalty_earned_points = refund_order_id.\
                                              total_loyalty_earned_points * -1
            total_loyalty_earned_amount = refund_order_id.\
                                              total_loyalty_earned_amount * -1
            refund_order_id.write({
                'total_loyalty_earned_points': total_loyalty_earned_points,
                'total_loyalty_earned_amount': total_loyalty_earned_amount,
                'total_loyalty_redeem_points': 0.00,
                'total_loyalty_redeem_amount': 0.00,
            })
        return order
