# -*- coding: utf-8 -*-

from odoo import models, fields


class ProductProduct(models.Model):
    _inherit = "product.product"

    loyalty_point = fields.Integer("Loyalty Point")
