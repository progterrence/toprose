# -*- coding: utf-8 -*-

def post_init_hook(cr, registry):
    query = "select state from ir_module_module where name = 'pos_loyalty'";
    cr.execute(query)
    res = cr.dictfetchall()
    if res and res[0].get('state', 0) and res[0]['state'] == 'uninstall':
        cr.execute("INSERT INTO loyalty_point(partner_id, points) SELECT id, "
            "abs(loyalty_points) FROM res_partner")