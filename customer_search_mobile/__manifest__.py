# -*- coding: utf-8 -*-
{
    'name': 'Customer Search Mobile',
    'summary': 'Customer Search Mobile',
    'version': '14.0.1.0.4',
    'author': 'Terrence Nzaywa',
    'license': 'AGPL-3',
    'maintainer': 'Terrence Nzaywa',
    'support': 'nza.terrence@gmail.com',
    'depends': [
         'sale'
    ],
    'data': [
        'customer.xml',
    ],
    'demo': [],
    'installable': True
}
