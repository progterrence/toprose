# -*- coding: utf-8 -*-
{
    'name': 'Toprose Customer Extenxtions',
    'category': 'Toprose Customer Extenxtions',
    'summary': 'This module allows customers earn loyalty points.',
    'author': 'Sunflower IT',
    'website': 'http://www.sunflowerweb.nl',
    'version': '14.0.1.0.0',
    'depends': [
        'sale'
    ],
    "data": [],
    'installable': True,
}
