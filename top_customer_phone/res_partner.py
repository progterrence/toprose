# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from odoo.exceptions import UserError


class TopCustomerExt(models.Model):
    _inherit = 'res.partner'

    first_name = fields.Char(
        string='First Name',
        compute="_first_name")

    
    @api.depends("name")
    def _first_name(self):
        for e in self:
            names = e.name.split(None, 1)
            if names:
                e.first_name = names[0]
            else:
                e.first_name = e.name

    @api.model
    def create(self, vals):
        if 'phone' in vals and vals['phone']:
            partner_with_same_phone = self.env['res.partner'].search([(
                'phone', '=', vals['phone'])])
            if partner_with_same_phone:
                raise UserError(_("Phone number already exists"))
        if 'mobile' in vals and vals['mobile']:
            partner_with_same_phone = self.env['res.partner'].search([(
                'mobile', '=', vals['mobile'])])
            if partner_with_same_phone:
                raise UserError(_("Mobile number already exists"))

        return super(TopCustomerExt, self).create(vals)

    
    def write(self, vals):
        if 'phone' in vals and vals['phone']:
            partner_with_same_phone = self.env['res.partner'].search([(
                'phone', '=', vals['phone'])])
            if partner_with_same_phone:
                raise UserError(_("Phone number already exists"))
        if 'mobile' in vals and vals['mobile']:
            partner_with_same_phone = self.env['res.partner'].search([(
                'mobile', '=', vals['mobile'])])
            if partner_with_same_phone:
                raise UserError(_("Mobile number already exists"))

        return super(TopCustomerExt, self).write(vals)


