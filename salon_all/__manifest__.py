
{
    'name': 'Salon All',
    'summary': 'All modules',
    'version': '14.0.1.0.4',
    'author': 'Terrence Nzaywa',
    'license': 'AGPL-3',
    'maintainer': 'Terrence Nzaywa',
    'support': 'nza.terrence@gmail.com',
    'depends': [
        'customer_birthday',
        'sfit_pos_loyalty',
        'sfit_pos_visits',
        'toprose_mtech_sms',
        'top_referral',
        'top_digital_receipt',
        'pos_custom_receipt',
        'staff_sale',
        'top_customer_phone'
    ],
    'demo': [],
    'data': [],
    'installable': True
}
